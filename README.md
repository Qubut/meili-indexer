# Meili indexer

Meili indexer is a Python script that enables synchronization of data between Meilisearch and Strapi CMS 

it uses poetry as its dependency management tool.



- `main.py`: should run every midnight to update Meilisearch_DB

 
## Installation

To initialize Meili Indexer, run:

```sh
poetry install
```

# Usage

#### Documentation

to build the documentation run

```sh
cd ./docs && poetry run sphinx-build -b html source build
```

Here's an example of how to use the script:

### From the command line:

- `poetry run python meili_indexer/main.py`
- 
### in another project:

```py

from meili_indexer import main as sync_data

# call sync_data function to start synchronization
sync_data()

```

# Contributing

Contributions are welcome! Please feel free to submit a pull request or open an issue if you find a bug or have a feature request.
License

This project is licensed under the MIT License - see the LICENSE file for details.