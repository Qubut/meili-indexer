"""
This This script indexes the data from Strapi into MeiliSearch.

Functions:
    - main: The main function that indexes the data into MeiliSearch.

"""

import asyncio
import os
import aiohttp
from logger import logger
from meilisearch.client import Client
from utils.utility import fetch_data
from config.endpoints import (
    aussteller_endpoint,
    stellenausschreibungen_endpoint,
    meili_url,
)


async def main():
    """
    Indexes data in MeiliSearch from the provided Strapi API endpoints asynchronously.

    :return: None
    """
    # Create a MeiliSearch client instance
    meili_client = Client(meili_url, os.environ.get("MEILI_MASTER_KEY"))

    # Create an aiohttp client session
    async with aiohttp.ClientSession() as session:
        # Fetch exhibitors data and job post data asynchronously
        ausstellers_task = asyncio.ensure_future(
            fetch_data(session, f"{aussteller_endpoint}")
        )
        stellenausschreibugen_task = asyncio.ensure_future(
            fetch_data(session, f"{stellenausschreibungen_endpoint}")
        )
        await asyncio.gather(ausstellers_task, stellenausschreibugen_task)

        # Get the results of the two tasks
        ausstellers = ausstellers_task.result()
        stellenausschreibugen = stellenausschreibugen_task.result()

        # Add 'studiengänge' list to the data dictionaries
        for a in ausstellers:
            a["studiengänge"] = [stud["studiengang"] for stud in a["studiengangs"]]
        for s in stellenausschreibugen:
            s["studiengänge"] = [stud["studiengang"] for stud in s["studiengangs"]]

        # Index the data
        logger.info("Indexing")
        for data, index_name in [
            (ausstellers, "aussteller"),
            (stellenausschreibugen, "stellenausschreibung"),
        ]:
            logger.info(f"creating index: {index_name}")
            index = meili_client.index(index_name)
            logger.info(f"deleting old documents from index: {index_name}")
            index.delete_all_documents()
            logger.info(f"Adding {len(data)} entries to index {index_name}")
            index.add_documents(data)

        # Update filterable attributes for the two indexes
        logger.info("Setting filterableAttributes")
        meili_client.index("stellenausschreibung").update_filterable_attributes(
            ["art", "studiengänge"]
        )
        meili_client.index("aussteller").update_filterable_attributes(["studiengänge"])
        logger.info("Setting sortable Attributes")
        meili_client.index("aussteller").update_sortable_attributes(["firma"])


if __name__ == "__main__":
    asyncio.run(main())
