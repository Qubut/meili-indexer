"""
This module contains functions for fetching data from a Strapi API.

Functions:
- fetch_data: Fetch data from a Strapi API endpoint.
"""

from aiohttp import ClientSession
from typing import Dict, Any, List
from config.endpoints import strapi_url
from logger import logger

async def fetch_data(session: ClientSession, endpoint: str) -> List[Dict[str, Any]]:
    """
    Fetch data from a Strapi API endpoint.

    Args:
        session (aiohttp.ClientSession): An aiohttp client session to use for the request.
        endpoint (str): The endpoint to query for data.

    Returns:
        List[Dict[str, Any]]: A list of dictionaries, where each dictionary represents a single record in the response
            data.

    Raises:
        ValueError: If the request fails or returns a status code outside the 2xx range.
    """
    # Construct the full URL for the API endpoint.
    url = f"{strapi_url}/{endpoint}"

    # Use the aiohttp client session to send a GET request to the endpoint.
    async with session.get(url) as response:
        # Raise an exception if the response has a status code outside the 2xx range.
        if 199 > response.status or response.status >= 400:
            raise ValueError(f"Failed to fetch data {response.status}")

        # If the response is valid, parse the JSON data from the response body and extract the 'data' field.
        data = (await response.json())['data']

        # Log the number of entries fetched from the endpoint.
        logger.info(f'fetched {endpoint}: {len(data)} entries')

        # Return the list of dictionaries representing the fetched records.
        return data