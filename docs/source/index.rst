.. Meili Indexer documentation master file, created by
   sphinx-quickstart on Wed Mar 15 13:43:39 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Meili Indexer's documentation!
=========================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   installation
   usage
   meili_indexer
   utility


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
