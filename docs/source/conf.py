# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
import sphinx_rtd_theme

# Path to the directory containing your Python modules
sys.path.insert(0, os.path.abspath('../../meili_indexer'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Meili Indexer'
copyright = '2023, Abdullah Ahmed'
author = 'Abdullah Ahmed'
release = '1.0.0'

# -- General configuration ---------------------------------------------------

templates_path = ['_templates']
exclude_patterns = []

autodoc_docstring_signature = True
# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
]

# -- Options for HTML output -------------------------------------------------

html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]


html_static_path = ['_static']
