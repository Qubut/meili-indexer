Usage
=====

To use the Strapi Profairs Synchroniser, 
run

`cd repo_location/meili_indexer && poerty run python main.py`

or set a cronjob.
